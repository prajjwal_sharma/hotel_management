<?php

namespace App\Console\Commands;

use App\Book;
use Illuminate\Console\Command;
use Symfony\Component\Finder\Tests\Comparator\DateComparatorTest;

class UpdateBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate =Date('Y-m-d');
        Book::where('check_out', $currentDate)->update(['status','0']);
    }
}
