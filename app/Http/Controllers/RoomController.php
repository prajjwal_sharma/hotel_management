<?php

namespace App\Http\Controllers;

use App\Cat;
use App\Room;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class RoomController extends Controller
{
    public function __construct(Room $room){
        return $this->room = $room;
    }
    public function getShowroom(){
        $rooms = $this->room->all();
        $title = 'show room';
        return view('admin.room.show_room')
            ->with('title', $title)
            ->with('rooms', $rooms);
  }

    public function getNewroom(){
        $cat = Cat::all();
        $title = 'New Room';
        return view('admin.room.new_room')
            ->with('title', $title)
            ->with('cat', $cat);
    }

    public function postNewroom(Request $request){
        $this->validate($request,
            [
                'room_no'=>'required|numeric|min:1|max:255|unique:rooms',
                'cat'=>'required',
                'photo'=>'required'
            ]);

        $file = $request->file('photo');
        $move = $file->move(public_path('upload'),$file->getClientOriginalName());
        /*check if photo exit*/
        $pExit =$this->room->all()->where('photo', $file->getClientOriginalName());
        if($pExit->count() > 0){
            return Redirect('room/newroom')->with(['phexit' => 'Photo already exit']);
        }

        $this->room->room_no = $request->get('room_no');
        $this->room->cat_id = $request->get('cat');
        $this->room->photo = $file->getClientOriginalName();
        $this->room->save();

        return Redirect('room/showroom');
    }

    public function getEditroom($id){
        $title = "edit";
        $cat = Cat::all();
        $room = $this->room->find($id);
        return view('admin.room.editroom')->with('title', $title)
            ->with('cat', $cat)
            ->with('room', $room);
    }

    public function postEditroom(Request $request){
        $editRoom = $this->room->find($request->get('id'));
        $this->validate($request,
            [
                'room_no'=>'required|numeric|min:1|max:1080',
                'cat'=>'required',
            ]);

        $exit =$this->room
            ->where('room_no', $request->get('room_no'))
            ->where('id', '!=', $request->get('id'))
            ->exists();
        if($exit){
            return Redirect('room/editroom/'.$request->get('id'))->with(['msg'=>'room_no already exit']);
        }

        $editRoom->room_no = $request->get('room_no');
        $editRoom->cat_id = $request->get('cat');
        if($request->file('photo')){
            $file = $request->file('photo');
            $move = $file->move(public_path('upload'),$file->getClientOriginalName());
            $photo = $file->getClientOriginalName();

            $pExit =$this->room
                ->where('photo', $file->getClientOriginalName())
                ->where('id', '!=', $request->get('id'))
                ->exists();
            if($pExit){
                return Redirect('room/editroom/'.$editRoom->id)->with(['phexit' => 'Photo already exit']);
            }
        }else{
            $photo = $editRoom->photo;
        }
        $editRoom->photo = $photo;
        $editRoom->save();
        return Redirect('room/showroom');
    }

    public function getDelroom($id){
        $delroom = $this->room->find($id);
        $delroom->delete();
        return Redirect('room/showroom');
    }
}
