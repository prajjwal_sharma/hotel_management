<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminHomeController extends Controller
{
    public function getDash(){
        $title ="Admin Dash";
        return view('admin.dash')->with('title', $title);
    }

    public function getLogout(){
        Auth::logout();
        return redirect('admin/login');
    }
}
