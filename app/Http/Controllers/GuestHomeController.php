<?php

namespace App\Http\Controllers;

use App\Book;
use App\Guest;
use App\Room;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GuestHomeController extends Controller
{
    public function __construct(Book $book){
        return $this->book = $book;
    }

    public function getLogout(){
        Auth::logout();
        return Redirect('guest/login');
    }

    public function getGuestdash(){
        $title ="Guest Dash";
        return view('users.user_dash')
            ->with('title', $title);
    }

    public function getHome(){
        $room = Room::all();
        $title = "this is user dashboard";
        return view('users.home')
            ->with('room', $room)
            ->with('title', $title);
    }

    public function getBook($room_no){
        $email = Auth::user()->email;
        $title = "book now";
        return view('users.booknow')
            ->with('email', $email)
            ->with('room_no', $room_no)
            ->with('title', $title);
    }

    public function postBook(Request $request){
        $this->validate($request,
            [
            'check_in'=>'required|date',
            'check_out'=>'required|date'
            ]);

        if($request->get('check_in') < date('Y-m-d')){
           return Redirect('ghome/book/'.$request->get('room_no'))->with(['date'=>'date should be from tommorrow']);
        }

        $this->book->room_no = $request->get('room_no');
        $this->book->email = $request->get('email');
        $this->book->check_in = $request->get('check_in');
        $this->book->check_out = $request->get('check_out');
        if($this->book->save()){
            Room::where('room_no', $this->book->room_no)->update(['status'=>'1']);
            Book::where('room_no', $this->book->room_no)->update(['status'=>'1']);
         }

        return Redirect('ghome/home')
            ->with(['msg'=>'successfully Booked']);
    }

    public function getCancel($room_no){
        Room::where('room_no', $room_no)->update(['status'=>'0']);
        Book::where('room_no', $room_no)->update(['status'=>'0']);
        return Redirect('ghome/home');
    }

   public function getHistory(){
        $email = Auth::user()->email;
        $book = Book::where('email', $email)->get();
        $title = "this is book";
        return view('users.history')
            ->with('book', $book)
            ->with('title', $title);
    }
}
