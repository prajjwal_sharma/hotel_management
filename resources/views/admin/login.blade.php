@extends('admin_master')
@section('section')
    <div class="col-md-4"></div>
     <div class="col-md-4">
         <div class="jumbotron">
            <h2 class="text-center text-primary bg-danger" style="padding: 10px 0; margin-bottom: 25px">Admin Login</h2>
            <form action="{{url('admin/login')}}" method="post">
            {{csrf_field()}}
                   <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control"/>
                    <span style="color: red">{{$errors->first('email')}}</span>
                   </div>
                   <div class="form-group">
                       <label for="password">Password</label>
                       <input type="password" id="password" name="password" class="form-control"/>
                  </div>
                  <span style="color: red">{{$errors->first('password')}}</span><br/>
                  <input type="submit" value="Login" class="btn btn-danger"/>
            </form>
        </div>
     </div>
@endsection