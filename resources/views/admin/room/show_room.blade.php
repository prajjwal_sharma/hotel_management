@extends('......admin_master')
@section('section')
    <a href="{{url('room/newroom')}}" class="btn btn-info">Add New Room</a>
    <table class="table table-stripped">
        <tr>
            <th>Id</th>
            <th>Room Number</th>
            <th>Catagory</th>
            <th>Photo</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
         @foreach($rooms as $room)
        <tr>
            <td>{{$room->id}}</td>
            <td>{{$room->room_no}}</td>
            <td>{{$room->cat->where('id', $room->cat_id)->value('name')}}</td>
            <td><img src="{{url('upload/'.$room->photo)}}" alt="" width="100px" class="img-responsive"/></td>
            <td>{{$room->status}}</td>
            <td><a href="{{url('room/editroom/'.$room->id)}}" class="btn btn-sm btn-info">Edit</a>
            <a href="{{url('room/delroom/'.$room->id)}}" class="btn btn-sm btn-danger">Delete</a></td>
         </tr>
         @endforeach
    </table>
@endsection