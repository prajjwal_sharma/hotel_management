@extends('......admin_master')
@section('section')
    <a href="{{url('cat/newcat')}}" class="btn btn-info">Add New Catagory</a>
    <table class="table table-stripped">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
         @foreach($cat as $ca)
        <tr>
            <td>{{$ca->id}}</td>
            <td>{{$ca->name}}</td>
            <td>Rs. {{$ca->price}}</td>
            <td>{{$ca->description}}</td>
            <td><a href="{{url('cat/editcat/'.$ca->id)}}" class="btn btn-sm btn-info">Edit</a>
            <a href="{{url('cat/delcat/'.$ca->id)}}" class="btn btn-sm btn-danger">Delete</a></td>
         </tr>
         @endforeach
    </table>
@endsection